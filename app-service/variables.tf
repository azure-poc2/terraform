# variable "applications_registration" {
#   type = list(string)
# }

variable "location" {
  description = "The Azure Region where the Resource Group should exist"
  type        = string
}

variable "rg_name" {
  description = "The name of the Resource Group"
  type        = string
}

variable "sku_name" {
  description = "The SKU for the plan"
  type        = string
}

variable "os_type" {
  description = "The O/S tyoe for the App Service"
  type        = string
}

variable "webapp_plan" {
  description = "The App Service plan name"
  type        = string
}

variable "webapp_name" {
  description = "The App Service name"
  type        = string
}