module "appservice" {
  source = "./app-service"
  # applications_registration = var.applications_registration
  location    = var.location
  rg_name     = var.rg_name
  sku_name    = var.sku_name
  os_type     = var.os_type
  webapp_name = var.webapp_name
  webapp_plan = var.webapp_plan
}