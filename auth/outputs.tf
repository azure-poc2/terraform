output "sp_client_secret" {
  value       = azuread_service_principal_password.app.value
  sensitive   = true
  description = "The Service Principal Client Secret"
}

output "sp_object_id" {
  value       = azuread_service_principal.app.object_id
  description = "The Service Principal Object ID"
}


output "subscription_id" {
  value       = data.azurerm_subscription.primary.subscription_id
  description = "The subscription ID"
}

output "tenant_id" {
  value       = data.azurerm_client_config.client.tenant_id
  description = "The Tenant ID"
}


output "client_id" {
  value       = azuread_application_registration.app.client_id
  description = "The CLient ID"
}

